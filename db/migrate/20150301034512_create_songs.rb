class CreateSongs < ActiveRecord::Migration
  def change
    create_table :songs do |t|
      t.integer :aid
      t.string :artist
      t.integer :duration
      t.integer :owner_id
      t.string :title
      t.string :url
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
