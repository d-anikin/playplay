class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :uid
      t.string :href
      t.string :first_name
      t.string :last_name
      t.string :nickname
      t.string :photo
      t.string :token

      t.timestamps null: false
    end
  end
end
