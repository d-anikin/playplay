# Coding: utf-8
# Фоновый поток следит за песнями
if defined? START_WATCHER && START_WATCHER
  play_thread = Thread.new do
    puts "Watcher start"
    tick = 0
    while true
      tick += 1
      Song.current.stop! if Song.current && Song.current.stop_at < Time.now
      Song.current.kill! if Song.current && Song.current.scores < -2
      if Song.current.nil? && song = Song.first
        song.play!
      end

      if tick % 30 == 0
        puts "User rest"
        User.all.each do |user|
          user.rest!
        end
      end
      tick = 0 if tick > 1000
      sleep 1.second
    end
  end

  at_exit do
    play_thread.kill
  end
end
