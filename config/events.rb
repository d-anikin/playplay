WebsocketRails::EventMap.describe do
  subscribe :client_connected, :to => Websocket::SessionController, :with_method => :client_connected
  subscribe :client_disconnected, :to => Websocket::SessionController, :with_method => :client_disconnected
  namespace :users do
    subscribe :get_list, :to => Websocket::UsersController, :with_method => :get_list
    subscribe :toggle_listen, :to => Websocket::UsersController, :with_method => :toggle_listen
  end
  namespace :songs do
    subscribe :get_list, :to => Websocket::SongsController, :with_method => :get_list
    subscribe :add, :to => Websocket::SongsController, :with_method => :add
    subscribe :remove, :to => Websocket::SongsController, :with_method => :remove
    subscribe :like, :to => Websocket::SongsController, :with_method => :like
    subscribe :dislike, :to => Websocket::SongsController, :with_method => :dislike
  end
end
