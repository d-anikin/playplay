Rails.application.routes.draw do
  match '/auth/:provider/callback', to: 'sessions#create', via: [:get,:post]
  match '/logout', to: 'sessions#destroy', via: [:get,:post]
  get 'login', to: 'sessions#login'

  post 'vk', to: 'vk_request#make'
  post 'vk_add', to: 'vk_request#add'
  root 'main#index'
end
