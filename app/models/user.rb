# == Schema Information
#
# Table name: users
#
#  id         :integer          not null, primary key
#  uid        :string
#  href       :string
#  first_name :string
#  last_name  :string
#  nickname   :string
#  photo      :string
#  token      :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class User < ActiveRecord::Base
  include Redis::Objects
  include WsNotification
  ENERGY_START = 100

  validates :uid, :first_name, :last_name, presence: true

  belongs_to :channel
  has_many :songs

  # after_save :notify_updated

  counter :exp
  counter :energy, :start => User::ENERGY_START
  counter :rated_count, :start => 0
  value   :title
  set     :abilites # умения доступные пользователю

  def update_by_omni!(auth)
    self.uid = auth.uid
    self.first_name = auth.info.first_name
    self.last_name = auth.info.last_name
    self.nickname = auth.info.nickname
    self.photo = auth.info.image
    self.token = auth.credentials.token
    self.href = auth.info.urls.Vkontakte
    self.save!
  end

  # Проверка может ли пользователь что-то делать
  def can?(abilite)
    abilites.member? abilite
  end

  # Установить что пользователь может abilite
  def can!(abilite)
    abilites.add abilite
    ws_update
  end

  # Установить что пользователь не может abilite
  def cannot!(abilite)
    abilites.delete abilite
    ws_update
  end

  def lvl
    (Math.log((exp.value/300.0+1)**5)).floor
  end

  def lvl_exp
    exp.value - ((Math.exp(lvl)**0.2)*300-300).round
  end

  def lvl_req
    ((Math.exp(lvl+1)**0.2)*300-300).round - ((Math.exp(lvl)**0.2)*300-300).round
  end

  def max_energy
    User::ENERGY_START + lvl * 7
  end

  def name
    [first_name, last_name].join(' ')
  end

  # spend_energy_and_give_exp
  def seage!(energy_to_spend, exp_to_receive)
    return spend_energy(energy_to_spend) && give_exp(exp_to_receive) && ws_update
  end

  # give_energy_and_give_exp
  def geage!(energy_to_receive, exp_to_receive)
    return give_energy(energy_to_receive) && give_exp(exp_to_receive) && ws_update
  end

  def give_energy(energy_to_receive)
    energy_to_receive = energy_to_receive.round
    if energy.value + energy_to_receive > max_energy
      energy.value = max_energy
    elsif energy.value + energy_to_receive < 0
      energy.value = 0
    else
      energy.increment(energy_to_receive)
    end
    true
  end

  def give_energy!(energy_to_receive)
    give_energy(energy_to_receive) && ws_update
    true
  end

  def spend_energy(energy_to_spend)
    energy_to_spend = energy_to_spend.round
    raise ActionError.new("Нужно больше энергии! Требуется #{energy_to_spend}") if energy.value < energy_to_spend
    energy.decrement(energy_to_spend)
    true
  end

  def spend_energy!(energy_to_spend)
    return spend_energy(energy_to_spend) && ws_update
  end

  def give_exp(exp_to_receive)
    # рандомный бонус не более 20%
    exp_to_receive += exp_to_receive * 0.05 * rand
    exp_to_receive = exp_to_receive.round
    old_lvl = lvl
    if exp.value + exp_to_receive < 0
      exp.value = 0
    else
      exp.increment(exp_to_receive)
      # энергия восстанавливается при повышении уровня
      energy.value = max_energy if old_lvl < lvl
    end
    true
  end

  def give_exp!(exp_to_receive)
    return give_exp(exp_to_receive) && ws_update
  end

  def rest!
    give_energy!(max_energy * APP_VALUES_ENERGY['ratio_rest'])
    true
  end

  def to_jbuilder(user)
    Jbuilder.new do |json|
      json.extract! self, :id, :uid, :href, :photo
      json.name "#{first_name} #{last_name}"
      json.title title.value
      json.energy energy.value
      json.extract! self, :lvl, :lvl_exp, :lvl_req, :max_energy
      json.abilites abilites.members if id == user.id
      json.connected WebsocketRails.users[id].connected?
    end
  end

  def action!(*args)
    options = args.extract_options!
    action_name = args[0]
    song = options[:song]
    case action_name
    when :add_song
      seage!(
        song.duration * APP_VALUES_ENERGY['ratio_song_added'],
        song.duration >= 180 ? APP_VALUES_EXP['added_song'] : (song.duration * APP_VALUES_EXP['added_song'] / 180).round
      )
      rated_count.value = 0 if song.duration >= 60
    when :remove_song
      seage!(
        song.duration * APP_VALUES_ENERGY['ratio_song_removed'],
        APP_VALUES_EXP['removed_song']
      )
    when :like_song
      seage!(
        rated_count.value * APP_VALUES_ENERGY['rated_song'],
        APP_VALUES_EXP['rated_song']
      )
      rated_count.increment
    when :dislike_song
      seage!(
        rated_count.value * APP_VALUES_ENERGY['rated_song'],
        APP_VALUES_EXP['rated_song']
      )
      rated_count.increment
    end
    true
  end


end
