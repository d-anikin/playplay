# == Schema Information
#
# Table name: songs
#
#  id         :integer          not null, primary key
#  aid        :integer
#  artist     :string
#  duration   :integer
#  owner_id   :integer
#  title      :string
#  url        :string
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Song < ActiveRecord::Base
  include Redis::Objects
  include WsNotification
  validates :aid, :artist, :duration, :owner_id, :title, :url, :user_id, presence: true
  validates :duration, :numericality => { :greater_than => 0 }
  validates :aid, uniqueness: true
  belongs_to :user
  cattr_accessor :current

  counter :int_play_at
  list :votes, :marshal => true
  set :vote_ids
  counter :scores

  before_destroy :destroy_song

  def to_jbuilder(user_dest)
    Jbuilder.new do |json|
      json.extract! self, :id, :aid, :owner_id, :artist, :title, :duration, :url, :play_at, :stop_at
      json.playing playing?
      if vote_ids.member?(user_dest.id) || user_id == user_dest.id
        json.votes votes
        json.scores scores.value
        json.user do
          json.name user.name
          json.photo user.photo
        end
      end
      json.actions do
        json.remove (user_dest.id == user_id)
        json.vote can_vote?(user_dest)
      end
    end
  end

  def playing?
    int_play_at.exists?
  end

  def play_at
    playing? ? Time.at(int_play_at.value) : nil
  end

  def stop_at
    playing? ? Time.at(int_play_at.value + duration) : nil
  end

  def play!
    if Song.current.nil?
      Song.current = self
      int_play_at.value = Time.now.to_i unless playing?
      ws_update
      return true
    end
    return false
  end

  def stop!
    user.geage!(scores.value * APP_VALUES_ENERGY['rated_song'], scores.value * APP_VALUES_EXP['stoped_song'])
    self.destroy
    true
  end

  def kill!
    user.geage!(scores.value * APP_VALUES_ENERGY['rated_song'], APP_VALUES_EXP['killed_song'])
    self.destroy
    true
  end

  def can_vote?(user)
    return user_id != user.id && !vote_ids.member?(user.id)
  end

  def vote!(user, like)
    raise ActionError.new('Вы не можете голосовать за эту песню') unless can_vote?(user)
    vote_ids << user.id
    votes << { user_id: user.id, name: user.name, photo: user.photo, like: like }
    like ? scores.increment : scores.decrement
    ws_update
    true
  end

private
  def destroy_song
    Song.current = nil if Song.current && Song.current.id == id
  end

end
