module WsNotification
  extend ActiveSupport::Concern

  included do
    after_create :ws_create
    after_save :ws_update
    after_destroy :ws_destroy
  end
  def to_jbuilder(user)
    Jbuilder.new
  end

  def ws_create
    WebsocketRails.users.each do |ws_user|
      ws_user.send_message("#{self.class.name.underscore}.create", self.to_jbuilder(ws_user.user).attributes!)
    end
    true
  end

  def ws_update
    WebsocketRails.users.each do |ws_user|
      ws_user.send_message("#{self.class.name.underscore}.update", self.to_jbuilder(ws_user.user).attributes!)
    end
    true
  end

  def ws_destroy
    WebsocketRails.users.each do |ws_user|
      ws_user.send_message("#{self.class.name.underscore}.destroy", self.id)
    end
    true
  end


end
