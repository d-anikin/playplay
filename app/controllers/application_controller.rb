class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :check_current_user

  def current_user
    @current_user ||= session[:user_uid].present? ? User.find_by_uid(session[:user_uid]) : nil
  end
  helper_method :current_user

  private
    def check_current_user
      if current_user.nil?
        session[:user_uid] = nil
        redirect_to login_path
      end
    end
end
