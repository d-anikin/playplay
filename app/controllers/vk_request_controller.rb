class VkRequestController < ApplicationController
  before_action :set_vk
  layout false

  def make
    case params[:vk_action]
    when 'audio.get'
      render json: @vk.audio.get(params[:vk_params])
    when 'audio.getPopular'
      render json: @vk.audio.getPopular(params[:vk_params])
    when 'audio.getRecommendations'
      render json: @vk.audio.getRecommendations(params[:vk_params])
    when 'audio.search'
      render json: @vk.audio.search(params[:vk_params])
    else render json: { result: 'action unknow'}
    end
  end

  def add
    @vk = VkontakteApi::Client.new(current_user.token)
    responce = @vk.audio.add({ owner_id: params[:owner_id], audio_id: params[:aid] })
    head :ok
  end

private
  def set_vk
    @vk = VkontakteApi::Client.new(current_user.token)
  end
end
