require 'net/http'

class SessionsController < ApplicationController
  skip_before_filter :verify_authenticity_token
  skip_before_filter :check_current_user
  layout false

  def login
  end

  def create
    @user = User.find_by_uid(request.env['omniauth.auth']["uid"]) || User.new
    @user.update_by_omni!(request.env['omniauth.auth'])
    session[:user_uid] = @user.uid
    redirect_to root_path
  end

  def destroy
    session.delete(:user_id)
    redirect_to login_path
  end
end
