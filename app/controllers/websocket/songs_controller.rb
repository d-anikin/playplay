# Coding: utf-8
class Websocket::SongsController < Websocket::BaseController
  before_filter do
    trigger_failure({ message: 'Current user is nil' }) and return false if current_user.nil?
  end

  def get_list
    list = []
    Song.all.each do |song|
      list.push(song.to_jbuilder(current_user).attributes!)
    end
    trigger_success(list)
  end

  def add
    max_count = (3 + current_user.lvl / 5).round
    raise ActionError.new("Вы не можете добавить больше #{max_count} песен в плейлист!") if current_user.songs.count >= max_count
    song = Song.new(song_params)
    if song.valid? && current_user.action!(:add_song, song: song) && song.save
      trigger_success({result: :ok})
    else
      raise ActionError.new('Вы не можете добавить эту песню')
    end
  end

  def remove
    song = current_user.songs.find(message[:id])
    if song && current_user.action!(:remove_song, song: song) && song.destroy!
      trigger_success({result: :ok})
    else
      raise ActionError.new('Вы не можете удалить эту песню')
    end
  end

  def like
    song = Song.find(message[:id])
    if !song.can_vote?(current_user)
      raise ActionError.new('Вы не можете голосовать за эту песню')
    elsif current_user.action!(:like_song, song: song) && song.vote!(current_user, true)
      trigger_success({result: :ok})
    else
      raise ActionError.new('Что-то пошло не так, мы все умрем!')
    end
  end

  def dislike
    song = Song.find(message[:id])
    if !song.can_vote?(current_user)
      raise ActionError.new('Вы не можете голосовать за эту песню')
    elsif current_user.action!(:dislike_song, song: song)  && song.vote!(current_user, false)
      trigger_success({result: :ok})
    else
      raise ActionError.new('Что-то пошло не так, мы все умрем!')
    end
  end

private
  def song_params
    message
      .slice(:aid, :artist, :duration, :owner_id, :title, :url)
      .tap { |x| x[:user_id] = current_user.id } # User.last.id
  end
end
