class Websocket::UsersController < Websocket::BaseController
  before_filter do
    trigger_failure({ message: 'Current user is nil' }) and return if current_user.nil?
  end

  def get_list
    list = []
    WebsocketRails.users.each do |ws_user|
      list.push(ws_user.user.to_jbuilder(current_user).attributes!)
    end
    trigger_success(list)
  end

  def toggle_listen
    current_user.can?('listen') ? current_user.cannot!('listen') : current_user.can!('listen')
  end
end

