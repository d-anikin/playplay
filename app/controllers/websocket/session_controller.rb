class Websocket::SessionController < Websocket::BaseController
  def client_connected
    connection.close! and return if current_user.nil?
    send_message :connected, { current_uid: current_user.uid }
    current_user.ws_update
  end

  def client_disconnected
    current_user.ws_update
  end
end
