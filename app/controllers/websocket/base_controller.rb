class Websocket::BaseController < WebsocketRails::BaseController
  before_action do
    # ActiveRecord::Base.connection.clear_query_cache
    true
  end
  after_action do
    ActiveRecord::Base.connection.close
    true
  end

  around_filter :rescue_request


  def rescue_request
    yield
  rescue ActionError => e
    trigger_failure({ result: :action_error, message: e.message })
  end
end
