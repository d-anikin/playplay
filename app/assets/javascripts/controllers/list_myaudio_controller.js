(function(window, angular, undefined) {
  'use strict';

  ListMyAudioController.$inject = ['$scope', 'myAudio', 'songs'];
  function ListMyAudioController($scope, myAudio, songs) {
    $scope.songs = songs;
    $scope.service = myAudio;
  }

  angular.module('appPlay').controller('ListMyAudioController',ListMyAudioController);
})(window, angular);
