(function(window, angular, undefined) {
  'use strict';

  ListPopularController.$inject = ['$scope', 'popularAudio', 'songs'];
  function ListPopularController($scope, popularAudio, songs) {
    $scope.songs = songs;
    $scope.service = popularAudio;
  }

  angular.module('appPlay').controller('ListPopularController',ListPopularController);
})(window, angular);
