(function(window, angular, undefined) {
  'use strict';

  PlayerController.$inject = ['$scope', '$interval', 'users', 'songs'];
  function PlayerController($scope, $interval, users, songs) {
    var playingId = false;
    $scope.listening = users.currentCan('listen');
    $scope.users = users;
    $scope.currentSong = songs.current;

    function calcTimeLeft() {
      if ($scope.currentSong && moment($scope.currentSong.stop_at) > moment()) {
        $scope.timeLeft = Math.abs(moment().diff($scope.currentSong.stop_at, 'seconds'));
        $scope.elapsedTime = Math.abs(moment().diff($scope.currentSong.play_at, 'seconds'));
        var iTmp = $scope.elapsedTime*100/$scope.currentSong.duration;
        $scope.bar.css('width', iTmp+'%');
      }
      else {
        $scope.timeLeft = 0;
        $scope.elapsedTime = 0;
      }
    }
    $interval(calcTimeLeft, 1000);

    function play() {
      calcTimeLeft();
      if ($scope.currentSong &&
          $scope.listening &&
          playingId != $scope.currentSong.id &&
          $scope.elapsedTime < $scope.currentSong.duration) {
        playingId = $scope.currentSong.id;
        $scope.jplayer.jPlayer("setMedia", { mp3: $scope.currentSong.url });
        $scope.jplayer.jPlayer("play", $scope.elapsedTime);
      }
    }

    function stop() {
      playingId = false;
      $scope.jplayer.jPlayer("stop");
    }

    $scope.toggleListening = function(){
      users.call('toggle_listen', {});
    };

    songs.onChangedCurrent(function(song){
      $scope.currentSong = song;
      if ($scope.currentSong) { play(); }
      else { stop(); }
    });

    users.onChangedCurrent(function(user){
      if ($scope.listening != users.currentCan('listen')) {
        $scope.listening = users.currentCan('listen');
        if ($scope.listening) { play(); }
        else { stop(); }
      }
    });
  }

  angular.module('appPlay').controller('PlayerController',PlayerController);
})(window, angular);
