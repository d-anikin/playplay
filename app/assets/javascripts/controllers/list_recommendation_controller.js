(function(window, angular, undefined) {
  'use strict';

  ListRecommendationController.$inject = ['$scope', 'recommendationAudio', 'songs'];
  function ListRecommendationController($scope, recommendationAudio, songs) {
    $scope.songs = songs;
    $scope.service = recommendationAudio;
  }

  angular.module('appPlay').controller('ListRecommendationController',ListRecommendationController);
})(window, angular);
