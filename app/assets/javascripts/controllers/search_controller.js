(function(window, angular, undefined) {
  'use strict';

  SearchController.$inject = ['$scope', 'searchAudio', 'songs'];
  function SearchController($scope, searchAudio, songs) {
    var typeTimer = false;
    $scope.songs = songs;
    $scope.service = searchAudio;
    $scope.title = "Найти аудиозапись";

    $scope.search = function() {
      if (typeTimer) {clearTimeout(typeTimer);}
      typeTimer = setTimeout(searchRequest, 500);
    };

    function searchRequest(){
      searchAudio.search(searchAudio.query);
    }
  }

  angular.module('appPlay').controller('SearchController',SearchController);
})(window, angular);
