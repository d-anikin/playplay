(function(window, angular, undefined) {
  'use strict';

  ListPlayingController.$inject = ['$scope', 'songs', 'users', 'VK'];
  function ListPlayingController($scope, songs, users, VK) {
    $scope.songs = songs;
    $scope.users = users;
    $scope.vk = VK;
    $scope.totalTime = function() {
      var total = 0;
      _.forEach($scope.songs.list, function(song){ total += song.duration; });
      return total
    }
  }

  angular.module('appPlay').controller('ListPlayingController',ListPlayingController);
})(window, angular);
