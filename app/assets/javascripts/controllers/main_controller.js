(function(window, angular, undefined) {
  'use strict';

  MainController.$inject = ['$scope', '$state', 'users', 'websocketService'];
  function MainController($scope, $state, users, websocketService) {
    $scope.users = users;
    $scope.websocketService = websocketService;
    $scope.isState = function(states){
      return $state.includes(states);
    };
  }

  angular.module('appPlay').controller('MainController',MainController);
})(window, angular);
