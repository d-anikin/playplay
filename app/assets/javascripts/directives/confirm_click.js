(function(window, angular, undefined) {
  'use strict';
  angular.module('appPlay').directive('confirmClick', [
    function(){
      return {
        link: function (scope, element, attr) {
          var msg = attr.ConfirmClick || "Are you sure?";
          var clickAction = attr.confirmClick;
          element.bind('click',function (event) {
            if ( window.confirm(msg) ) { scope.$eval(clickAction) }
          });
        }
      };
  }]);
})(window, angular);
