(function(window, angular, undefined) {
  'use strict';

  // player.$inject = [];
  function player() {
    return {
      templateUrl: 'player.html',
      controller: 'PlayerController',
      link: function(scope, element) {
        scope.jplayer = $('<div id="jplayer" style="display:none"/>');
        element.prepend(scope.jplayer);
        scope.jplayer.jPlayer({
          swfPath: "/swf",
          supplied: "mp3, m4a, oga"
        });
        scope.bar = element.find('[role="progressbar"]');
      }
    };
  }

  angular.module('appPlay').directive('player',player);
})(window, angular);
