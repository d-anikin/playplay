(function(window, angular, undefined) {
  'use strict';

  songsService.$inject = ['websocketService', '$state'];
  function songsService(websocketService, $state) {
    var _this = this,
        changedCurrentCallbacks = [];
    this.list = [];

    websocketService.connect().then(function(data){
      websocketService.trigger('songs.get_list', {},
        function(data) {
          _this.list = data;
          _this.changeCurrent(_.find(_this.list, { playing: true }));

          websocketService.bind('song.create', function(song) {
            var index = _.findIndex(_this.list, { id: song.id });
            if (index >=0) { _this.list[index] = song; }
            else _this.list.push(song);
            if (song.playing) { _this.changeCurrent(song); }
          });

          websocketService.bind('song.update', function(song) {
            var index = _.findIndex(_this.list, { id: song.id } );
            if (index >=0) { _this.list[index] = song; }
            else _this.list.push(song);
            if (song.playing) { _this.changeCurrent(song); }
          });

          websocketService.bind('song.destroy', function(id) {
            var index = _.findIndex(_this.list, { id: id });
            _this.list.splice(index,1);
            if (_this.current && _this.current.id == id) { _this.changeCurrent(undefined); }
          });
        },
        function(data) { console.error('WS Error songsService', data);}
      );
    });

    this.onChangedCurrent = function(callback) { changedCurrentCallbacks.push(callback); };
    this.changeCurrent = function(song) {
      var oldCurrent = _this.current;
      _this.current = song;
      angular.forEach(changedCurrentCallbacks, function(callback) { callback(_this.current, oldCurrent); });
    };

    this.call = function(path, msg, success, failure) {
      websocketService.trigger('songs.'+path, msg, success, failure);
    };

    this.add = function(song) {
      websocketService.trigger('songs.add', song,
        function(data) { /* $state.go('main.playing'); */ },
        function(data) { console.error('WS Error songs.add', data); }
      );
    };

    this.remove = function(song) {
      websocketService.trigger('songs.remove', song,
        function(data) {},
        function(data) { console.error('WS Error songs.remove', data); }
      );
    };

    this.like = function(song) {
      websocketService.trigger('songs.like', song,
        function(data) {},
        function(data) { console.error('WS Error songs.like', data); }
      );
    };

    this.dislike = function(song) {
      websocketService.trigger('songs.dislike', song,
        function(data) {},
        function(data) { console.error('WS Error songs.dislike', data); }
      );
    };
  }

  angular.module('appPlay').service('songs', songsService);
})(window, angular);
