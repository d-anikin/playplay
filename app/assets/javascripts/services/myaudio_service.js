(function(window, angular, undefined) {
  'use strict';

  myAudio.$inject = ['users', 'VK'];
  function myAudio(users, VK) {
    var service = this;
    this.records = [];
    this.loaded = true;
    this.offset = 0;

    this.more = function() {
      service.loading = true;
      VK.call('audio.get', { owner_id: users.currentUID, offset: service.offset }, function(response) {
        service.count = response.shift();
        service.offset += response.length;
        service.loaded = (service.count <= service.offset);
        service.records.push.apply(service.records,response);
        service.loading = false;
      });
    };
    this.more();
  }

  angular.module('appPlay').service('myAudio', myAudio);
})(window, angular);
