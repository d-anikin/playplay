(function(window, angular, undefined) {
  'use strict';

  vkService.$inject = ['$http'];
  function vkService($http) {
    var service = this;
    this.call = function(path, params, success) {
      $http.post('/vk', {
        vk_action: path,
        vk_params: params
      }).then(
        function(responce) { success(responce.data); },
        function(responce) { console.error('VK Error', responce.data); }
      );
    };

    this.add = function(song) {
      $http.post('/vk_add', song).then(
        function(responce) { song.copied = true; },
        function(responce) { console.error('VK add Error', responce.data); }
      );
    };
  }


  angular.module('appPlay').service('VK', vkService);
})(window, angular);
