(function(window, angular, undefined) {
  'use strict';

  usersService.$inject = ['websocketService'];
  function usersService(websocketService) {
    var _this = this,
        changedCurrentCallbacks = [];

    this.list = [];

    websocketService.connect().then(function(data){
      _this.currentUID = data.current_uid;
      websocketService.trigger('users.get_list', {},
        function(data) {
          _this.list = data;
          _this.changeCurrent(_.find(_this.list, { uid: _this.currentUID }));

          websocketService.bind('user.update', function(user) {
            var index = _.findIndex(_this.list, { id: user.id });
            if (index >=0) { _this.list[index] = user; }
            else _this.list.push(user);
            if (_this.currentUID == user.uid) { _this.changeCurrent(user); }
          });

          websocketService.bind('user.destoy', function(id) {
            var index = _.findIndex(_this.list, { id: user.id });
            _this.list.splice(index,1);
            if (_this.current && _this.current.id == id) { _this.changeCurrent(undefined); }
          });
        },
        function(data) { console.error('WS Error usersService', data);}
      );
    });

    this.onChangedCurrent = function(callback) { changedCurrentCallbacks.push(callback); };
    this.changeCurrent = function(user) {
      var oldCurrent = _this.current;
      _this.current = user;
      angular.forEach(changedCurrentCallbacks, function(callback) { callback(_this.current, oldCurrent); });
    };

    this.currentCan = function(abilite) {
      return _this.current && _this.current.abilites && _.includes(_this.current.abilites, abilite);
    };

    this.call = function(path, msg, success, failure) {
      websocketService.trigger('users.'+path, msg, success, failure);
    };

  }

  angular.module('appPlay').service('users', usersService);
})(window, angular);
