(function(window, angular, undefined) {
  'use strict';

  searchAudio.$inject = ['VK'];
  function searchAudio(VK) {
    var service = this;
    this.records = [];
    this.loaded = true;

    this.search = function(query) {
      service.loading = true;
      service.query = query;
      service.records.length = 0;
      VK.call('audio.search', { q: service.query }, function(response) {
        service.count = response.shift();
        service.offset = response.length;
        service.loaded = (service.count <= service.offset);
        service.records.push.apply(service.records, response);
        service.loading = false;
      });
    };

    this.more = function() {
      service.loading = true;
      VK.call('audio.search', { q: service.query, offset: service.offset }, function(response) {
        response.shift();
        service.offset += response.length;
        service.loaded = (service.count <= service.offset);
        service.records.push.apply(service.records,response);
        service.loading = false;
      });
    };
  }

  angular.module('appPlay').service('searchAudio', searchAudio);
})(window, angular);
