(function(window, angular, undefined) {
  'use strict';

  websocketService.$inject = ['$q', '$rootScope', '$interval', '$http'];
  function websocketService($q, $rootScope, $interval, $http) {
    var _this = this,
        ws,
        reconnectTickCount,
        deferred = $q.defer();
    this.reconnected = undefined;
    this.connect = function() { return deferred.promise; };

    ws = new WebSocketRails(window.location.host + '/websocket');
    ws.bind('connected', function(data) { deferred.resolve(data); });
    ws.bind('connection_closed', function() {
      _this.reconnected = 15;
      $interval(reconnectTick, 1000);
    });

    function reconnectTick(){
      if (_this.reconnected < 2) {
        _this.reconnected = 15;
        $http.get('/').success(function(){
          window.location.reload();
        });
      }
      _this.reconnected--;
    }

    this.bind = function(event_name, callback) {
      ws.bind(event_name, function(data) {
        $rootScope.$apply(function() { callback(data); });
      });
    };

    this.trigger = function(event_name, data, success_callback, failure_callback) {
      ws.trigger(event_name, data,
        function(data) {
          $rootScope.$apply(function() { success_callback(data); });
        },
        function(data) {
          if (data.result == 'action_error') {
            alert(data.message);
          }
          $rootScope.$apply(function() { failure_callback(data); });
        });
    };
  }

  angular.module('appPlay').service('websocketService', websocketService);

})(window, angular);
