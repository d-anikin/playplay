(function(window, angular, undefined) {
  'use strict';

  angular.module('appPlay').filter('duration', function() {
    return function(seconds) {
      return moment.duration(seconds, "seconds").format("mm:ss", {trim: false});
    };
  });
})(window, angular);