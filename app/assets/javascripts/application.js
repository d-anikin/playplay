//= require jquery
//= require lodash
//= require moment
//= require moment-duration-format
//= require angular
//= require angular-ui-router
//= require angular-rails-templates
//= require jquery.jplayer.min
//= require websocket_rails/main
//= require bootstrap
//= require_tree ../templates
//= require_self
//= require_tree .

(function(window, angular, undefined) {
  'use strict';
  angular.module('appPlay', ['ui.router', 'templates'])
    .config(['$httpProvider', '$stateProvider', '$urlRouterProvider',
      function ($httpProvider, $stateProvider, $urlRouterProvider) {
        $httpProvider.defaults.headers.common['X-CSRF-Token'] = $('meta[name=csrf-token]').attr('content');
        $httpProvider.defaults.headers.post['Content-Type'] = 'application/json; charset=utf-8';
        $urlRouterProvider.otherwise("/");
        $stateProvider
          .state('main', { abstract: true, controller: 'MainController', templateUrl: 'main.html'})
          .state('main.playing', { parent: 'main', controller: 'ListPlayingController', templateUrl: 'list_playing.html'})
          .state('main.search', { parent: 'main', controller: 'SearchController', templateUrl: 'songs.html'})
          .state('main.myAudio', { parent: 'main', controller: 'ListMyAudioController', templateUrl: 'songs.html'})
          .state('main.popular', { parent: 'main', controller: 'ListPopularController', templateUrl: 'songs.html'})
          .state('main.recommendation', { parent: 'main', controller: 'ListRecommendationController', templateUrl: 'songs.html'})
        ;
      }
    ])
    .run(['$state', function ($state) {
      $state.go('main.playing');
    }]);
})(window, angular);

